#!/usr/bin/env node
import clipboard from "clipboardy";
import program from "commander";
import chalk from "chalk";
import nirGenerator from "nir-generator";

import saveNir from "./utils/saveNir.js";
const log = console.log;
program.version("1.0.0").description("Simple Nir Generator");

program.option("-s, --save", "save social number to nir.txt").parse();

const { save } = program.opts();

// use
let myNIR = nirGenerator.generateNirTypeA();

//  Copy to clipboard
clipboard.writeSync(myNIR);

//  Save to file
if (save) {
	saveNir(myNIR);
}

// You can validate a NIR using
if (myNIR.match(nirGenerator.nirRegex)) {
	log(chalk.blue("Generated Nir: ") + chalk.bold(myNIR));
	log(chalk.yellow("Nir copied to clipboard"));
}
