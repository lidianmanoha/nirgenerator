import fs from "fs";
import path from "path";
import os from "os";
import chalk from "chalk";

const saveNir = (nir) => {
	// full_path = os.path.realpath(__file__);
	console.log(process.cwd());
	fs.open(path.join(process.cwd(), "nirs.txt"), "a", (e, id) => {
		fs.write(id, nir + os.EOL, null, "utf-8", () => {
			fs.close(id, () => {
				console.log(chalk.green("nir saved to nir.txt"));
			});
		});
	});
};

export default saveNir;
